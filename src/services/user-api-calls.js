import axios from "./Http";

export const loginUser = (body) => {
  const url = `user/create_token/`;
  return axios.post(url, body);
};
export const registerUser = (body) => {
  const url = `user/create_user/`;
  return axios.post(url, body);
};
//profile page
export const userInformation = () => {
  const url = `user/profile/`;
  return axios.get(url);
};
export const updateUserInfo = (body) => {
  const url = `user/profile/`;
  return axios.patch(url, body);
};
//ticket page
export const userTicketList = (id = "") => {
  const url = `user/ticket/${id ? `${id}/` : ''}`;
  return axios.get(url);
};
export const sendNewTicket = (body) => {
  const url = `user/ticket/`;
  return axios.post(url, body);
};
export const sendResponseTicket = (id,body) => {
  const url = `user/ticket/${id}/`;
  return axios.post(url, body);
};
//course page
export const userCoursesList = () => {
  const url = `user/product/`;
  return axios.get(url);
};
