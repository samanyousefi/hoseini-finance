import axios from "./Http";

export const getCoursersList = () => {
  const url = `index/product/`;
  return axios.get(url);
};
export const getCourseDetail = (name) => {
  const url = `index/product/${name}`;
  return axios.get(url);
};
export const getCourseFiles = (id) => {
  const url = `index/product/${id}/productfile/`;
  return axios.get(url);
};
export const getQuestionsList = () => {
  const url = `index/question/`;
  return axios.get(url);
};
export const sendQuestions = (body) => {
  const url = `index/question/`;
  return axios.post(url, body);
};
export const getAboutMedias = () => {
  const url = `index/about-image-video/`;
  return axios.get(url);
};
