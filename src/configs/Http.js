import axios from "axios";
import { useRouter } from "next/router";
import { store } from "../store/store";
const baseUrl = process.env.REACT_APP_BASE_URL;
const userAuthForms = ["login", "register", "forgot-pass", "change-pass"];
axios.interceptors.request.use(
  (config) => {
    const { pathname } = useRouter();
    const accessToken = store.getState().user.access;
    config.baseURL = baseUrl;
    // console.log('config req is',config)
    if (
      pathname.split("/").slice(1).length > 1 &&
      userAuthForms.indexOf(pathname.split("/").slice(1)[1]) < 0
    ) {
      config.headers["Authorization"] =
        "Bearer " + store.getState().user.access;
    }
    return config;
  },
  (error) => {
    throw error;
  }
);
axios.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    throw error;
  }
);
export default {
  axios,
  get: axios.get,
  post: axios.post,
  patch: axios.patch,
  delete: axios.delete,
};
