export const convertDate = (dateTime, type) => {
  let date = new Date(dateTime.slice(0, 10)).toLocaleDateString("fa-IR");
  let time = dateTime.slice(11, 16);
  if (type === "date") {
    return `${date}`;
  } else if (type === "time") {
    return `${time}`;
  } else {
    return `${date} - ${time}`;
  }
};
export const convertSecondIntoTime = (time, second = false) => {
  let hrs = ~~(time / 3600);
  let mins = ~~((time % 3600) / 60);
  let secs = ~~time % 60;
  if (second) {
    return ` ${hrs} : ${mins} : ${secs} `;
  } else {
    return ` ${hrs} : ${mins} `;
  }
};
export const decodeTimeForTimer = (time, second = false) => {
  let day = ~~(time / 3600 / 24);
  let hrs = ~~(time / 3600) - day * 24;
  let mins = ~~((time % 3600) / 60);
  let secs = ~~time % 60;
  return { day, hrs, mins, secs };
};
export const formatNumber = (num) => {
  return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
};
