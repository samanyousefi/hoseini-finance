import React, { useEffect, useState } from "react";
import Styles from "./Header.module.scss";
import { useRouter } from "next/router";
import Link from "next/link";
import { Container, Row, Col } from "react-bootstrap";
//store
import { useSelector } from "react-redux";
//components
import MenuItem from "./components/MenuItems/MenuItem";
import CustomizeButton from "../../components/form/button/Button";

const Header = ({ activeTheme }) => {
  const { pathname } = useRouter();
  const user = useSelector((state) => state.user);
  const [pageScroll, setPageScroll] = useState();
  const [openHeader, setOpenHeader] = useState(false);

  useEffect(() => {
    setOpenHeader(false);
  }, [pathname]);

  return (
    <Container className={Styles.container}>
      <Row
        className={`${Styles.headerRow} align-items-center justify-content-start p-relative`}
      >
        <Col className={Styles.imageLogo}>
          <Link href="/">
            <a>
              <img src="/images/HeaderLogo.png" alt="hoseini finance" />
            </a>
          </Link>
        </Col>

        <Col className={`${Styles.menuBox} h-100`}>
          <div
            className={Styles.menuButton}
            onClick={() => setOpenHeader((prev) => !prev)}
          >
            {openHeader ? (
              <img src="/images/closeIc.png" alt="close" />
            ) : (
                <img src="/images/menuIc.png" alt="menu" />
              )}
          </div>

          <div className={Styles.logoResponsive}>
            <img src="/images/logo_responsive.png" alt="Hoseini Finance" />
          </div>

          <div
            className={`${Styles.menuItem} transition-5 ${
              openHeader ? Styles.open : " "
            }
            ${
              activeTheme === "homeTheme" ? Styles.linkWhite : Styles.linkDark
            }`}
          >
            <ul className="flex-row align-items-stretch h-100 m-0">
              <MenuItem title="دوره ها" url="/courses" />
              <MenuItem title="چرا ما؟" url="/about" />
              <MenuItem title="پرسش‌های متداول" url="/faq" />
              <MenuItem title="تماس با ما" url="/contact-us" />
              <MenuItem title="قوانین و مقررات" url="/rules" />
              <MenuItem title="گواهینامه دوره" url="/certificate" />
            </ul>
          </div>
        </Col>

        <Col className={`${Styles.userButton} `}>
          {!(pathname.split("/").slice(1)[0] === "user") ? (
            <div className={`${Styles.loginPc} text-left`}>
              <CustomizeButton outlined>
                <Link href={user && user.access ? "/user/profile" : "/user"}>
                  <a
                    className={
                      activeTheme === "homeTheme" ? "text-white" : null
                    }
                  >
                    {user && user.access ? "پروفایل من" : "ورود / ثبت نام"}
                  </a>
                </Link>
              </CustomizeButton>
            </div>
          ) : null}
        </Col>
      </Row>
    </Container>
  );
};
export default Header;
