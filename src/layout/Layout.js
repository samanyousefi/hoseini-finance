import React, { useRef, useState, useEffect } from "react";
import Styles from "./Layout.module.scss";
import { useRouter } from "next/router";
import { StickyContainer, Sticky } from "react-sticky";
//components
import Footer from "./footer/Footer";
import Header from "./header/Header";
import { Headset, TelephoneFill } from "react-bootstrap-icons";
import MobileMenu from "./mobileMenu/Menu";
const Layout = ({ activeTheme, children }) => {
  const [scrollFlag, setScrollFlag] = useState(false);
  const headerRef = useRef();
  useEffect(() => {
    if (activeTheme !== "hidden") {
      if (window) {
        window.addEventListener("scroll", handleScroll);
      }
      return () => window.removeEventListener("scroll", handleScroll);
    }
  }, [activeTheme]);

  const handleScroll = (e) => {
    const bodyScrollTop = window.scrollY;
    if (bodyScrollTop >= 100) {
      headerRef.current.classList.add("FixedMenu");
    } else {
      if (activeTheme === "homeTheme") {
        headerRef.current.classList.remove("FixedMenu");
      }
    }
  };

  return (
    <>
      <div className={Styles.supportRsp}>
        <a className="text-light" href="tel:02144258068">
          <TelephoneFill size={20} />
        </a>
      </div>
      {activeTheme !== "hidden" ? (
        <div
          ref={headerRef}
          className={`${Styles.headerContainer} ${
            !(activeTheme === "homeTheme") ? "FixedMenu" : ""
          }`}
        >
          <Header activeTheme={activeTheme} />
        </div>
      ) : null}
      <div
        className={`${Styles.content} ${
          activeTheme === "hidden" ? "h-100" : Styles.marginTop
        }
			${activeTheme === "homeTheme" ? Styles.homePage : ""}
			`}
      >
        {children}
      </div>
      {activeTheme === "staticTheme" || activeTheme === "homeTheme" ? (
        <div className={Styles.footerContainer}>
          <Footer />
        </div>
      ) : null}
      {activeTheme === "staticTheme" || activeTheme === "homeTheme" || activeTheme === "hidden" ? ( <MobileMenu/>
      ) : null}
    </>
  );
};
export default Layout;
