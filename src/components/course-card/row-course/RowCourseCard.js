import React from "react";
import { Row, Col } from "react-bootstrap";
import Styles from "./RowCourseCard.module.scss";
import Link from 'next/link';
import CustomizeButton from "../../form/button/Button";
import {
  ChevronLeft,
  PersonCircle,
  LayersHalf,
  Clock,
  PlayCircle,
} from "react-bootstrap-icons";
const RowCourseCard = ({ data }) => {
  return (
    <div className={`${Styles.CourserCard} text-right p-3 mb-4`}>
      <Row className="align-items-center">
        <Col lg={3} md={3} sm={12} className={Styles.img}>
          <img src={`http://77.237.77.250:8080/${data.image}`} />
        </Col>
        <Col lg={9} md={9} sm={12} className={Styles.desc}>
          <h2 className={`${Styles.title} mb-3`}>
            دوره جامع معامله گری و تحلیل تکنیکال
          </h2>
          <div
            className={`${Styles.info} align-items-end d-flex flex-wrap justify-content-between`}
          >
            <ul>
              <li>
                <PersonCircle className={Styles.icon} />
                {` مدرس: ${data.authors[0].name} `}
              </li>
              <li>
                <LayersHalf className={Styles.icon} /> {data.level}
              </li>
              <li className="FaNum">
                <Clock className={Styles.icon} />{" "}
                {`زمان کل دوره : ${data.total_time}`}
              </li>
              <li>
                <PlayCircle className={Styles.icon} /> نمایش دوره به صورت آنلاین
              </li>
            </ul>
            <button className="btn btn-outline-success btn-sm d-flex align-items-center logRegSubmit ">
              {" "}
              <Link href={`/courses/${data.id}`}>
                <a>
                  نمایش <ChevronLeft className={Styles.iconLeft} />
                </a>
              </Link>
            </button>
          </div>
        </Col>
      </Row>
    </div>
  );
};

export default RowCourseCard;

// <div className={Styles.imgCard}>
// <Card.Img variant="top" src="./images/courserPic.png" />
// <span className={Styles.enTitle}>Course of Tradingn and Technical Analysis</span>
// </div>
// <Card.Body>
// <Card.Text className={Styles.subTitle}>
//   <span className="ml-4">مبتدی تا پیشرفته</span>
//   {` `}
//   <span>مدت آموزش: 34ساعت</span>
// </Card.Text>
// {/* <Card.Title>{data.title}</Card.Title> */}
// <Card.Title className={Styles.title}>دوره جامع معامله گری و تحلیل تکنیکال</Card.Title>
// {/* <Card.Text>{data.desc_sm} </Card.Text> */}
// <Card.Text className={Styles.text}>
//   {' '}
//   امروز در دنیایی زندگی میکنیم که کسب و کار های آنلاین در آمد های بسیار بیشتری نسبت به کسب و کارهای
//   سنتی دارند{' '}
// </Card.Text>

// <div className="d-flex justify-content-between align-items-end">
//   <span>
//     {data.off_time ? (
//       <>
//         <div>
//           <span>${data.old_price}</span>
//           <span></span>
//         </div>
//         <div>{`${data.price} تومان`}</div>
//         {/*{`${data.price} تومان`} */}
//       </>
//     ) : (
//       <>
//         <div>
//           {`٢,٣٠٠,٠٠٠ `}
//           <span className={Styles.currency}>تومان</span>
//         </div>
//         {/*{`${data.price} تومان`} */}
//       </>
//     )}
//   </span>
//   <Button variant="outline-primary" className={Styles.button}>
//     مشاهده <ChevronLeft className={Styles.icon} />
//   </Button>
// </div>
// </Card.Body>
