import { Form, Row, Col } from 'react-bootstrap';
import Styles from './AuthTemplate.module.scss';
const AuthTemplate = ({ children, title, image, hiddenHs = false }) => {
	return (
		<div className={Styles.LogRegWrapper}>

			<Row className={`${Styles.LogRegBox} d-flex align-items-center `}>
				<Col className={`${Styles.background} text-center p-0`}>

					<div className={`${Styles.desc} f-100 `}>

						<h2 className={`${Styles.title}  mb-2`}>{title} </h2>
						{ !hiddenHs ? null : <p className={`${Styles.subTitle}`}>حسینی فایننس</p>}
						<p className="sm-text mb-2 mt-3">مسیر سرمایه‌گذاری خود را با ما آغاز کنید. </p>
						<a className="sm-text mb-2 text-light" href="/">بازگشت به سایت </a>
					</div>

					<div className={`${Styles.img} f-100 `}>
						<img src={image} />
					</div>

				</Col>
				<Col className={`${Styles.LogReg} px-0 py-3 d-flex justify-content-center flex-wrap`}>{children}</Col>
			</Row>
			<div className="clr"></div>
		</div>
	);
};

export default AuthTemplate;
