import React, { useState } from 'react';
import Styles from './SideBar.module.scss';
import { QuestionCircle } from 'react-bootstrap-icons';
import { cousers, users, support, setting } from './data';
import Link from 'next/link';
const SideBar = () => {
	const [openSubMenu, setOpenSubMenu] = useState([]);
	return (
		<div className={`sidebar ${Styles.sidebar} d-flex`}>
			<ul className={`${Styles.menu} `}>
				<li className="text-center cursor-pointer" onClick={() => setOpenSubMenu(cousers)}>
					<div>
						<QuestionCircle />
					</div>
					<div>دوره ها</div>
				</li>
				<li className="text-center cursor-pointer" onClick={() => setOpenSubMenu(users)}>
					<div>
						<QuestionCircle />
					</div>
					<div>مدیریت کاربران</div>
				</li>
				<li className="text-center cursor-pointer" onClick={() => setOpenSubMenu(support)}>
					<div>
						<QuestionCircle />
					</div>
					<div>پشتیبانی</div>
				</li>
				<li className="text-center cursor-pointer" onClick={() => setOpenSubMenu([])}>
					<Link href={`/admin/setting`}>
						<a>
							<div>
								<QuestionCircle />
							</div>
							<div>تنظیمات</div>
						</a>
					</Link>
				</li>
			</ul>
			{openSubMenu && openSubMenu.length ? (
				<ul className={`${Styles.submenu}`}>
					{openSubMenu.map((item) => {
						return (
							<li>
								<Link href={`/admin/${item.url}`}>
									<a>{item.title}</a>
								</Link>
							</li>
						);
					})}
				</ul>
			) : null}
		</div>
	);
};

export default SideBar;
