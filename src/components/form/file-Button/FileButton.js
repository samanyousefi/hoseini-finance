import { forwardRef } from "react";
import { Button, Form } from "react-bootstrap";

const CustomizeFileButton = forwardRef(({
  name,
  label = "",
  icon: Icon,
  outlined = false,
  isFullwidth = false,
  disable = false,
  isLoading = false,
  size = "md", //lg,sm,md
  className,
  children,
  onClick,
  handleSetImage,
  ...rest
}, ref) => {
  let accept = {
    binary: ["image/png", "image/jpeg"],
    text: ["text/plain", "text/css", "application/xml", "text/html"],
  };

  return (
    <Form.Group className="form-group-file">
      <Form.Label>
        {Icon ? <Icon/>:''}
        {label ?
          label
          : ""}
      </Form.Label>
      <Form.File
        // id="exampleFormControlFile1"
        as="button"
        name={name}
        // feedbackTooltip
        // feedback={errors.file}
        // isInvalid={!!errors.file}
        // onChange={(e) => handleAddImage('')}
        ref={ref}
      />
      {/* <input type="file" onChange={(e)=>handleSetImage(e)}/> */}
    </Form.Group>
  );
});
export default CustomizeFileButton;
