import { Modal, Button } from "react-bootstrap";
import { X } from "react-bootstrap-icons";
import Styles from "./Modal.module.scss";

const ModalComponent = ({ title, size = "lg", show, setShow, children }) => {
  return (
    <Modal
      className={Styles.modal}
      show={show}
      size={size}
      dialogClassName="modal-90w"
      aria-labelledby="example-custom-modal-styling-title"
    >
      {title ? (
        <Modal.Header className="align-items-center">
          <Modal.Title
            id="example-custom-modal-styling-title"
            className={Styles.title}
          >
            <span>{title}</span>
          </Modal.Title>
          <X
            onClick={() => setShow(false)}
            className={`${Styles.close} cursor-pointer`}
          />
        </Modal.Header>
      ) : (
        <div className="text-left">
          <X
            onClick={() => setShow(false)}
            className={`${Styles.close} cursor-pointer`}
          />
        </div>
      )}
      <Modal.Body>{children}</Modal.Body>
    </Modal>
  );
};

export default ModalComponent;

// <Modal  className={Styles.modal}
// show={show}
// size={size}
// dialogClassName="modal-90w"
// aria-labelledby="example-custom-modal-styling-title"
// >
// {title ? (
//   <Modal.Header className="align-items-center">
//     <Modal.Title id="example-custom-modal-styling-title" className={Styles.title}>
//       <span>{title}</span>
//     </Modal.Title>
//     <X  onClick={() => setShow(false)} className={`${Styles.close} cursor-pointer`}/>
//   </Modal.Header>
// ) : (
//   <div className="text-left">
//     <X onClick={() => setShow(false)} className={`${Styles.close} cursor-pointer`}/>
//   </div>
// )}
// <Modal.Body>{children}</Modal.Body>
// </Modal>
