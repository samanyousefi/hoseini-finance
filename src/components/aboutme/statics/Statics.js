import React from 'react';
import { Row, Col } from 'react-bootstrap';
import Styles from "./Statics.module.scss";
import { Star, PlayBtn, Person } from 'react-bootstrap-icons';

const statiData = [

    { icon: <Star/>, title: "تجربه کاری", number: "5 سال" },
    { icon: <PlayBtn/>, title: "دوره‌های خریداری شده", number: "14K" },
    { icon: <Person/>, title: "تعداد کاربران", number: "100K" }
]
const StaticsCards = () => {
    return (
        <>
            <Row className={` ${Styles.row} px-3 align-items-stretch f-100`}>
                {
                    statiData.map(item => {
                        return (
                            <Col lg={4} md={4} sm={4}  className={`${Styles.static} px-2 mb-2`}>
                                <div className={`${Styles.staticCards} border-green flex-row justify-content-between align-items-center py-2 px-3 h-100`}>
                                    <div className={Styles.staticNumber}>{item.number}</div>
                                    <div className={`${Styles.staticIcon} text-green`}>{item.icon}</div>
                                    <div className={`${Styles.staticTitles} f-100 mt-2`}>{item.title}</div>
                                </div>
                            </Col>  
                        )
                    })
                }
            </Row>
        </>
    )
}
export default StaticsCards;