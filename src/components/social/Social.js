import React from 'react';
import Styles from "./Social.module.scss";
import { Row, Col } from 'react-bootstrap';
const SocialCart = () => {
    return (
         
        <Row className={`${Styles.socialCart} my-3`}>
             
            <Col className="flex-row align-items-center justify-content-center">
                <p className={`${Styles.title} text-green ml-3`}>
                    حتما تو شبکه‌های اجتماعی دنبالم کن!
                </p>
                <ul className={Styles.icons}>
                    <li className="ml-4"><a className="flex-row align-items-center" ><img src="./images/instagram-ic.png" /></a></li>
                    <li className="ml-4"><a className="flex-row align-items-center" ><img src="./images/facebook-ic.png" /></a></li>
                    <li className="ml-4"><a className="flex-row align-items-center" ><img src="./images/linkedin-ic.png" /></a></li>
                    <li className="ml-4"><a className="flex-row align-items-center" ><img src="./images/twitter-ic.png" /></a></li>
                    <li className="ml-4"><a className="flex-row align-items-center" ><img src="./images/telegrem-ic.png" /></a></li>
                    <li><a className="flex-row align-items-center" ><img src="./images/aparat-ic.png" /></a></li>
                </ul>
            </Col>
         </Row>
         
    )
}




export default SocialCart;