import React from "react";
import Styles from "./PulseAnimationButton.module.scss";
const PulseAnimationButton = ({ icon: Icon, size, className }) => {
  return (
    <div  className={`${Styles.animationButton} ${className}`}>
      <span className={Styles.pulseButton2}>
        <span className={Styles.pulseButton}>{Icon ? <Icon /> : "?"}</span>
      </span>
    </div>
  );
};
export default PulseAnimationButton;

//sample
{
  /* <PulseAnimationButton
        icon={() => (
          <PlayCircleFilled style={{ color: "green", fontSize: 80 }} />
        )}
      /> */
}
