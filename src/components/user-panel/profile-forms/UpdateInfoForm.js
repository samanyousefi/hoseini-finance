import { useState, useEffect } from "react";
import { Person, Lock, Envelope } from "react-bootstrap-icons";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { UpdateInfoSchema } from "../../../configs/formValidateSchema";
import { updateUserInfo } from "../../../services";
//store
import { useSelector, useDispatch } from "react-redux";
import { UpdateInfoAction } from "../../../store/user/actions";
//components
import CustomizeInput from "../../form/input/Input";
import CustomizeButton from "../../form/button/Button";

const UpdateInfoForm = ({ successHandle }) => {
  const state = useSelector((state) => state.user);
  const dispatch = useDispatch();
  const [serverError, setServerError] = useState();
  const { handleSubmit, register, errors, reset } = useForm({
    resolver: yupResolver(UpdateInfoSchema),
  });
  const formData = new FormData();
  useEffect(() => {
    resetForm();
  }, [state]);
  const resetForm = () => {
    reset(
      {
        name: state.name,
        email: state.email,
      },
      {
        // errors: true, // errors will not be reset
        // dirtyFields: true, // dirtyFields will not be reset
        // isDirty: true, // dirty will not be reset
        // isSubmitted: false,
        // touched: false,
        // isValid: false,
        // submitCount: false,
      }
    );
  };
  const handleChangeData = async ({ name, email }) => {
    if (name !== state.name) formData.append("name", name);
    if (email !== state.email) formData.append("email", email);
    try {
      const { status, data } = await updateUserInfo(formData);
      if (status === 200) {
        successHandle(data.message);
        resetForm();
        dispatch(UpdateInfoAction({ name, email }));
      }
    } catch (error) {
      console.log(error);
    } finally {
    }
  };
  return (
    <>
      <CustomizeInput
        size="md"
        icon={() => <Person />}
        name="name"
        ref={register()}
        label="نام و نام خانوادگی "
        errorMessage={errors.name && errors.name.message}
      />
      <CustomizeInput
        size="md"
        icon={() => <Envelope />}
        label="آدرس ایمیل "
        ref={register()}
        name="email"
        errorMessage={errors.email && errors.email.message}
      />
      <div className="text-left mt-4">
        <CustomizeButton
          size="sm"
          onClick={handleSubmit(handleChangeData)}
          className="logRegSubmit"
        >
          ثبت تغییرات
        </CustomizeButton>
      </div>
    </>
  );
};

export default UpdateInfoForm;
