import { useEffect, useState } from "react";
import ReactPaginate from "react-paginate";
import Styles from "./Pagination.module.scss";
import { ArrowLeft, ArrowRight } from "react-bootstrap-icons";

const Pagination = ({
  totalRecords = 0,
  pageLimit = 5,
  pageNeighbours = 1,
  handleChangePage
}) => {
  const [totalPages, setTotalPages] = useState(0);
  useEffect(() => {
    setTotalPages(Math.ceil(totalRecords / pageLimit));
  }, []);
  return (
      <div className={`${Styles.pagination} d-flex align-items-center justify-content-center`}>
        <ReactPaginate
          previousLabel={<ArrowRight/>}
          nextLabel={<ArrowLeft/>}
          breakLabel={"..."}
          pageClassName={"FaNum"}
          breakClassName={"break-me"}
          pageCount={totalPages}
          marginPagesDisplayed={pageNeighbours}
          pageRangeDisplayed={pageNeighbours}
          onPageChange={(data)=>handleChangePage(data.selected*pageLimit)}
          containerClassName={"pagination"}
          subContainerClassName={"pages pagination"}
          activeClassName={"active"}
          />
    </div>
  );
};
export default Pagination;
