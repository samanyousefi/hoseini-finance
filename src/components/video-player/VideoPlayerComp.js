import { useEffect, useState, useRef } from "react";
import {usePlayer} from '../hooks/usePlayer'
import Styles from "./VideoPlayerComp.module.scss";
const VideoPlayerComponent = ({ src, autoplay = false, controls = true }) => {
  const playerRef = usePlayer({ src, controls, autoplay });

  return (
    <>
      <div data-vjs-player>
        <video ref={playerRef} className={`${Styles.videoPlay} video-js`} />
        {/* <video ref={playerRef} className={`${Styles.videoPlay} video-js`} /> */}
      </div>
    </>
  );
};

export default VideoPlayerComponent;
