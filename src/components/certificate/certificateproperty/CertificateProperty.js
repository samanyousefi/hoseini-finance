import React from 'react';
import Styles from "./CertificateProperty.module.scss";
import { Col } from 'react-bootstrap';


const certificateprop = [

    { number: "1", title: "بین المللی" },
    { number: "2", title: "قابلیت ترجمه" },
    { number: "3", title: "معتبر در خارج از کشور" },
    { number: "4", title: "قابلیت استعلام آنلاین" }
]
const CertificateProperty = () => {
    return (
        <>
            {
                certificateprop.map((item,index) => {
                    return (
                        <Col key={index} lg={3} md={6} sm={12}  className={`${Styles.certificatePropertyBox} px-5 text-center`}>
                            <span className={`${Styles.number} text-green transition`}>{item.number}</span>
                            <p className={`${Styles.text}`}>{item.title}</p>
                        </Col>
                    )
                })
            }
        </>

    );
};

export default CertificateProperty;
