import React from 'react';
import Styles from "./Comments.module.scss";
import { Col } from 'react-bootstrap';
import { Play, PlayFill } from 'react-bootstrap-icons';
import SwiperComponent from "../../swiper/Swiper";

const swiperParams = {
    slidesPerView: 3,
    spaceBetween: 10,
    freeMode: true,
    navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
    },
    breakpoints: {
        320: {
            slidesPerView: 1,
        },
        768: {
            slidesPerView: 2,
        },
        991: {
            slidesPerView: 3,
        },
    }
};

const commentsData = [
    { img: "/images/commnts.png", name: "محمد رمضانی", course: "دوره جامع استخراج ارزهای دیجیتال", date: "1399/05/23" },
    { img: "/images/commnts.png", name: "احمد رمضانی", course: "دوره جامع تحلیلگر اقتصادی", date: "1399/05/23" },
    { img: "/images/commnts.png", name: "علی رمضانی", course: "دوره جامع ارزهای دیجیتال", date: "1399/05/23" },

]
const commentsCard = ({playHandle}) => {
    return (
        <SwiperComponent className="align-items-stretch" params={swiperParams} >
            {
                commentsData.map((item,index) => {
                    return (
                        <Col lg={4} md={6} sm={12} key={index}>
                            <div className={`${Styles.commentsBox} h-100`}>
                                <img src={item.img} className={Styles.img} />
                                <div className={`${Styles.desc} p-3 `}>
                                    <span className={`${Styles.icon} bg-green transition`} 
                                    onClick={()=>playHandle()}><PlayFill className="icon" /></span>
                                    <p className="d-flex justify-content-between mt-3">
                                        <span>{item.name}</span>
                                        <span>{item.date}</span>
                                    </p>
                                    <p className=" m-0">
                                        {item.course}
                                    </p>
                                </div>
                            </div>
                        </Col>
                    )
                })
            }
        </SwiperComponent>

    );
};

export default commentsCard;
