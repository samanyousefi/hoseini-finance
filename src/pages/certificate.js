import { useState } from "react";

import Styles from "../styles/certificate.module.scss";
import { Container, Row, Col, Card } from "react-bootstrap";

//components
import HeadingCart from "../components/headingcard/HeadingCard";
import CertificateCards from "../components/certificate/certificatesteps/CertificateSteps";
import CertificatePropertyCards from "../components/certificate/certificateproperty/CertificateProperty";
import CommentsCard from "../components/certificate/comments/Comments";
import ModalComponent from "../components/modal/Modal";
import VideoPlayerComponent from "../components/video-player/VideoPlayerComp";
import BoronModal from "../components/modals/boron-modal/BoronModal";

const Certificate = () => {
  const [videoSrc, setVideoSrc] = useState("");
  const [titleModal, setTitleModal] = useState("");
  return (
    <>
      <ModalComponent
        size="xl"
        title={titleModal ? titleModal : "دوره جامع ارزهای دیجیتال"}
        show={videoSrc ? true : false}
        setShow={(status) => {
          setVideoSrc(status);
        }}
      >
        {videoSrc ? (
          <VideoPlayerComponent src="https://aspb21.cdn.asset.aparat.com/aparat-video/27dccba81bfebdfbc066398773923b6228455332-1080p.mp4?wmsAuthSign=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6ImRjZmNlYzZiNzVlNTc1NDMzZGNiYjI5YTUwYjcxNzM3IiwiZXhwIjoxNjExNDQzNTQyLCJpc3MiOiJTYWJhIElkZWEgR1NJRyJ9.-Rc5QJotGEz0B552v-Eph4yBM9BuDxTiyOiHPtfJg0Q" />
        ) : null}
      </ModalComponent>
      <HeadingCart
        className="h-100 p-wide "
        background="certificate.jpg"
        title="گواهینامه دوره"
      >
        <Row className="h-100 align-items-center">
          <Card className={Styles.aboutDesc}>
            <Card.Body>
              <p className={Styles.subTitle}>
                حضور حرفه ای تو بازارهای مالی همراه با مدرک برای شما هم
              </p>
              <span className={Styles.title}>آسونه!</span>
              <div className={Styles.cardDesc}>
                <p>مدرک بین المللی MPT اتریش</p>
                <p>داشتن این مدرک ، در داخل و خارج ازکشور (ایران) به منزله ی مهارت بالای دارنده این گواهینامه بوده و یک معیار مستند جهت اخذ کار مرتبط با گواهینامه مربوطه در کل جهان می باشد.</p>
                <p>جهت استعلام گواهینامه از طریق وب سایت <a href="http://www.mpt-academy.at" target="_blank">mpt-academy</a> با وارد کردن کد رجیستری ، اطلاعات پژوهشگر مشاهده می گردد.</p>
              </div>
            </Card.Body>
          </Card>
        </Row>
      </HeadingCart>
      {/* <BoronModal >hii</BoronModal> */}
      <div className="py-10 w-100">
        <Container>
          <Row className={Styles.certificate}>
            <Col xs={12} className="pb-6 text-center">
              <span className={`${Styles.title} text-green`}>
                مراحل اخذ گواهینامه
              </span>
              <p className={Styles.subTitle}>
                با همین سه مرحله گواهینامه بگیر و فعالیت مالی داشته باش
              </p>
            </Col>
            <Col xs={12}>
              <Row className="align-items-start justify-content-center mt-3">
                <CertificateCards />
              </Row>
            </Col>
          </Row>
        </Container>
      </div>
      <div className="py-10 w-100">
        <Container>
          <Row className={Styles.certificateProperty}>
            <Col xs={12} className="pb-6 text-center">
              <span className={`${Styles.title} text-green`}>
                ویژگی‌های گواهینامه
              </span>
            </Col>
            <Col xs={12}>
              <Row
                className={`${Styles.shadow} align-items-start justify-content-center mt-3`}
              >
                <CertificatePropertyCards />
              </Row>
            </Col>
          </Row>
        </Container>
      </div>
      <div className="py-10 w-100">
        <Container>
          <Row className={Styles.comments}>
            <Col xs={12} className="pb-6 text-center">
              <span className={`${Styles.title} text-green`}>
                نظرات کاربران دوره دیده
              </span>
            </Col>
            <Col xs={12}>
              <CommentsCard playHandle={() => setVideoSrc("play")} />
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
};

export default Certificate;
