import { useEffect, useState, useRef } from "react";
import { useRouter } from "next/router";
import { Container, Row, Col, Card, Tab, Nav, Form } from "react-bootstrap";
import { getCourseDetail, getCourseFiles } from "../../services";
import Styles from "./DeatailCourse.module.scss";
import {
  convertSecondIntoTime,
  formatNumber,
  decodeTimeForTimer,
} from "../../configs/utils";
//components
import PulseAnimationButton from "../../components/pulse-animation-button/PulseAnimationButton";
import VideoPlayerComponent from "../../components/video-player/VideoPlayerComp";
import TitlePanel from "../../components/title-panel/TitlePanel";
import CustomizeButton from "../../components/form/button/Button";
import Pagination from "../../components/paigination/Pagination";
import CustomizeInput from "../../components/form/input/Input";
import {
  PlayFill,
  PersonCircle,
  PlayCircle,
  LayersHalf,
  CollectionPlay,
  CardChecklist,
  ClockHistory,
  JournalBookmarkFill,
  PatchQuestion,
  ChatRightDots,
  FileEarmarkText,
  PlayBtn,
} from "react-bootstrap-icons";

const CourserDetail = (props) => {
  console.log(props, "props");
  const { query } = useRouter();
  const [course, setCourse] = useState({});
  const [filesCourse, setFilesCourse] = useState([]);
  const [dayTimer, setDayTimer] = useState();
  const [hrsTimer, setHrsTimer] = useState();
  const [minsTimer, setMinsTimer] = useState();
  const descRef = useRef();
  useEffect(() => {
    if (query) {
      handleGetDetailCourse(query.name);
    }
  }, [query]);
  useEffect(() => {
    descRef.current.innerHTML = course.desc;
    if (course && course.off_time) {
      const { day, mins, hrs } = decodeTimeForTimer(course.off_time);
      setDayTimer(day);
      setHrsTimer(hrs);
      setMinsTimer(mins);
      setInterval(() => {
        setMinsTimer((prev) => prev - 1);
        if (minsTimer === 0) {
          setHrsTimer((prev) => prev - 1);
          setMinsTimer(59);
        }
        if (hrsTimer === 0) {
          setDayTimer((prev) => prev - 1);
        }
      }, 60000);
    }
    return () => clearInterval();
  }, [course]);
  const handleGetDetailCourse = async (id) => {
    try {
      const { data, status } = await getCourseDetail(id);
      if (status === 200) {
        setCourse(data.data);
        handleGetFiles(id);
      }
    } catch (error) {
    } finally {
    }
  };
  const handleGetFiles = async (id) => {
    try {
      const { data, status } = await getCourseFiles(id);
      if (status === 200) {
        setFilesCourse(data.data);
      }
    } catch (error) {
    } finally {
    }
  };
  return (
    <Row>
      <div className={`${Styles.bgCourse} py-4`}>
        <Container>
          <Row className="align-items-stretch mb-5">
            <Col lg={12} className={`${Styles.navigation} mb-3`}>
              <ul className="d-flex justify-content-right align-items-center">
                <li>
                  <a href="/">صفحه اصلی</a>
                </li>
                <li>
                  <a href="/courses">دوره‌ها</a>
                </li>
                <li>
                  <span className="font-weight-bold">{course.title}</span>
                </li>
              </ul>
            </Col>
            <Col className={`${Styles.heading} `} lg={6}>
              <h1 className={`${Styles.title} h2 mb-2`}>{course.title}</h1>
              <p className={Styles.subTitle}>
                Comprehensive course of cryptocurrency
              </p>
            </Col>
          </Row>
        </Container>
      </div>

      <div className={Styles.videoRow}>
        <Container>
          <Row className="align-items-start mb-5 flex-row-reverse justify-content-center">
            <Col className={`${Styles.CourseInfo} pr-3`}>
              <div className={`${Styles.card} card p-4`}>
                <ul className={Styles.body}>
                  {course?.authors && course.authors.length ? (
                    <li
                      className={`${Styles.item} d-flex justify-content-right align-items-center mb-2`}
                    >
                      <span className={`${Styles.icon} ml-2`}>
                        <PersonCircle />
                      </span>
                      <span className={`${Styles.title} sm-text`}>
                        {` مدرس دوره : ${course.authors[0].name}`}
                      </span>
                    </li>
                  ) : null}
                  <li
                    className={`${Styles.item} d-flex justify-content-right align-items-center mb-2`}
                  >
                    <span className={`${Styles.icon} ml-2`}>
                      <LayersHalf />
                    </span>
                    <span className={`${Styles.title} sm-text`}>
                      {`  سطح دوره : ${course.level}`}
                    </span>
                  </li>
                  <li
                    className={`${Styles.item} d-flex justify-content-right align-items-center mb-2`}
                  >
                    <span className={`${Styles.icon} ml-2`}>
                      <PlayCircle />
                    </span>
                    <span className={`${Styles.title} sm-text`}>
                      نمایش دوره به صورت آنلاین
                    </span>
                  </li>
                  <li
                    className={`${Styles.item} d-flex justify-content-right align-items-center mb-2`}
                  >
                    <span className={`${Styles.icon} ml-2`}>
                      <CollectionPlay />
                    </span>
                    <span className={`${Styles.title} sm-text`}>
                      تعداد جلسات :{" "}
                      <span className="FaNum">{course.count_file}</span>
                    </span>
                  </li>
                  <li
                    className={`${Styles.item} d-flex justify-content-right align-items-center mb-2`}
                  >
                    <span className={`${Styles.icon} ml-2`}>
                      <CardChecklist />
                    </span>
                    <span className={`${Styles.title} sm-text`}>
                      {`پیش نیاز : ${course.prerequisites}`}
                    </span>
                  </li>
                  <li
                    className={`${Styles.item} d-flex justify-content-right align-items-center mb-2`}
                  >
                    <span className={`${Styles.icon} ml-2`}>
                      <ClockHistory />
                    </span>
                    <span className={`${Styles.title} sm-text FaNum ltr-dir`}>
                      {`زمان کل دوره : ${convertSecondIntoTime(
                        course.total_time,
                        true
                      )}`}
                    </span>
                  </li>
                  {/* <li
                    className={`${Styles.item} d-flex justify-content-right align-items-center mb-2`}
                  >
                    <span className={`${Styles.icon} ml-2`}>
                      <JournalBookmarkFill />
                    </span>
                    <span className={`${Styles.title} sm-text`}>
                      گواهینامه: دارد
                    </span>
                  </li> */}
                </ul>
                {course && course.price ? (
                  <>
                    <div className={Styles.borderDash}></div>
                    <div className={`${Styles.footer} mt-4`}>
                      <div className={`${Styles.price} d-flex flex-wrap`}>
                        {course?.old_price ? (
                          <p className="f-100 mb-2 text-center">
                            <span className={`${Styles.oldPrice} `}>
                              <span className={`${Styles.mainPrice} FaNum`}>
                                {formatNumber(course.old_price)}
                              </span>
                              <span className={Styles.subPrice}>تومان</span>
                            </span>
                          </p>
                        ) : null}
                        <p className="f-100 mb-2 text-center">
                          <span>
                            <span className={`${Styles.mainPrice} FaNum`}>
                              {formatNumber(course.price)}
                            </span>
                            <span className={Styles.subPrice}>تومان</span>
                          </span>
                        </p>
                      </div>
                      <div className={Styles.submit}>
                        <CustomizeButton isFullwidth size="md">
                          خرید دوره
                        </CustomizeButton>
                      </div>
                      <div
                        className={`${Styles.counterBox} mt-3 d-flex flex-wrap justify-content-center`}
                      >
                        <p className={`${Styles.text} mb-2 text-center`}>
                          زمان باقی‌مانده تا پایان فروش ویژه
                        </p>
                        <p
                          className={`${Styles.counter} f-100 mb-0 text-center d-flex justify-content-center`}
                        >
                          <span className="mx-2 FaNum">{dayTimer}</span>
                          <span className="mx-2 FaNum">{hrsTimer}</span>
                          <span className="mx-2 FaNum">{minsTimer}</span>
                        </p>
                      </div>
                      <div className={`${Styles.disCountCode} mt-3`}>
                        <form className={Styles.form}>
                          <input
                            className={Styles.input}
                            placeholder="کد تخفیف"
                          ></input>
                          <button className={Styles.button}>اعمال</button>
                        </form>
                      </div>
                    </div>
                  </>
                ) : null}
              </div>
            </Col>
            <Col className={`${Styles.CourseVideo} `}>
              <div className={`${Styles.cardImg} h-100`}>
                <PulseAnimationButton
                  className={Styles.icon}
                  icon={() => <PlayFill />}
                />
                <img src="/images/testt.png" />
              </div>
              <div className={`${Styles.card} mt-4 w-100 d-flex flex-wrap`}>
                <Col lg={12}>
                  <Card className="border-0">
                    <Card.Body className="p-0">
                      <Tab.Container
                        id="left-tabs-example"
                        defaultActiveKey="description"
                      >
                        <Nav
                          variant="pills"
                          className="flex-column courseNav py-2 px-3"
                        >
                          <div className={Styles.tabInformation}>
                            <Nav.Item className={Styles.items}>
                              <Nav.Link
                                className={`${Styles.link} px-0 ml-5`}
                                eventKey="description"
                              >
                                <span className={`${Styles.icon} icon`}>
                                  <FileEarmarkText />
                                </span>
                                <span className={`${Styles.title} title`}>
                                  توضیحات
                                </span>
                              </Nav.Link>
                            </Nav.Item>
                            <Nav.Item className={Styles.items}>
                              <Nav.Link
                                className={`${Styles.link} px-0 ml-5`}
                                eventKey="listMovies"
                              >
                                <span className={`${Styles.icon} icon`}>
                                  <CollectionPlay />
                                </span>
                                <span className={`${Styles.title} title`}>
                                  لیست دوره
                                </span>
                              </Nav.Link>
                            </Nav.Item>
                            {/* <Nav.Item className={Styles.items}>
                              <Nav.Link className={`${Styles.link} px-0`} eventKey="comment">
                                <span className={`${Styles.icon} icon`}><ChatRightDots /></span>
                                <span className={`${Styles.title} title`}>نظر کاربران</span>
                              </Nav.Link>
                            </Nav.Item> */}
                          </div>
                        </Nav>
                        <Row className={`${Styles.bodyInformation} py-4`}>
                          <Col>
                            <Tab.Content>
                              <Tab.Pane
                                eventKey="description"
                                className={Styles.description}
                                id="descriptionId"
                              >
                                <div ref={descRef}></div>
                              </Tab.Pane>
                              <Tab.Pane
                                eventKey="listMovies"
                                className={Styles.coursesList}
                              >
                                <Row className={`${Styles.list} py-3`}>
                                  <Col lg={12}>
                                    <TitlePanel title="محتویات دوره" />
                                  </Col>
                                  <Col>
                                    <Row>
                                      {filesCourse.map(
                                        (file, index) => {
                                          return (
                                            <Col lg={12} className="mb-3">
                                              <div
                                                className={`${Styles.items} transition p-3 d-flex align-items-center justify-content-between flew-wrap`}
                                              onClick={()=>console.log(file.id)}
                                              >
                                                <span className={`${Styles.title} FaNum`}>
                                                  <span className={Styles.icon}>
                                                    <PlayBtn />
                                                  </span>
                                                  {`قسمت ${index+1} | ${file.title}`}
                                                </span>
                                                <span
                                                  className={Styles.subTitle}
                                                >
                                                  <span className={Styles.desc}>
                                                    <span className="FaNum">
                                                      {convertSecondIntoTime(file.duration)}
                                                    </span>{" "}
                                                    دقیقه
                                                  </span>
                                                  <span className={Styles.desc}>
                                                    <span className="FaNum">
                                                      355
                                                    </span>{" "}
                                                    MB
                                                  </span>
                                                </span>
                                              </div>
                                            </Col>
                                          );
                                        }
                                      )}
                                    </Row>
                                    <div className="text-center w-100 mt-4">
                                      <Pagination
                                        totalRecords={filesCourse}
                                        pageLimit={10}
                                        pageNeighbours={2}
                                        handleChangePage={(data) =>
                                          console.log(data)
                                        }
                                      />
                                    </div>
                                  </Col>
                                </Row>
                              </Tab.Pane>
                              {/* <Tab.Pane eventKey="comment" className={Styles.comments}>
                              <Row className={`${Styles.list} py-3`}>
                                <Col lg={12}><TitlePanel title="محتویات دوره" /></Col>
                                <Col lg={12}>
                                <Row>
                                  <Col lg={12}>

                                  </Col>
                                </Row>
                                </Col>
                              </Row>
                              </Tab.Pane> */}
                            </Tab.Content>
                          </Col>
                        </Row>
                      </Tab.Container>
                    </Card.Body>
                  </Card>
                </Col>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    </Row>
  );
};
export default CourserDetail;
