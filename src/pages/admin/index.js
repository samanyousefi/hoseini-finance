import { useState } from 'react';
import { Form, Button, Row, Col } from 'react-bootstrap';
import {useRouter} from 'next/router';
import { useForm } from 'react-hook-form';
import { useDispatch } from 'react-redux';
import { adminLogin } from '../../services';
import { setTokenAction } from '../../store/user/actions';
import CustomizeInput from '../../components/form/input/Input';
import CustomizeButton from '../../components/form/button/Button';

const AdminLogin = () => {
	const router = useRouter()
	const dispatch = useDispatch();
	const [rememberMe, setRememberMe] = useState(false);
	const { register, handleSubmit, errors } = useForm();
	const onLogin = async (vals) => {
		vals = { ...vals, remember_me: rememberMe };
		try {
			const { status, data } = await adminLogin(vals);
			if (status === 200) {
				dispatch(setTokenAction(data.data));
				router.push('/admin/dashboard')
			}
		} catch (error) {
			console.log(error);
		}
	};
	return (
		<Row className="justify-content-center">
			<Col xs={3}>
				<form>
					<CustomizeInput type="text" label="نام کاربری" name="username" ref={register()} />
					<CustomizeInput type="text" label="رمز عبور" name="password" ref={register()} />
					<CustomizeButton isFullwidth onClick={handleSubmit(onLogin)} size="lg">
						ورود
					</CustomizeButton>
				</form>
			</Col>
		</Row>
	);
};
export default AdminLogin;
