import { useEffect, useState } from "react";
import { Container, Row, Col, Table, Button } from "react-bootstrap";
import Link from "next/link";
import { categories } from "../../../services";
const Category = () => {
  const [list, setList] = useState([]);
  useEffect(() => {
    getList();
  }, []);
  const getList = async () => {
    try {
      const { data } = await categories();
      setList(data.data);
    } catch (error) {
    } finally {
    }
  };
  // const addCategoryFunc = async (vals) => {
  // 	try {
  // 		const {status} = await ?(vals);
  // 	} catch (error) {
  // 	} finally {
  // 	}
  // };
  // const updateCategoryFunc = async (id,vals) => {
  // 	try {
  // 		const {status} = await ?(id,vals);
  // 	} catch (error) {
  // 	} finally {
  // 	}
  // };
  // const delCategoryFunc = async (id) => {
  // 	try {
  // 		const {status} = await ?(id);
  // 	} catch (error) {
  // 	} finally {
  // 	}
  // };
  return (
    <Container>
      <Row className="mb-6 align-items-center justify-content-between">
        <Col>search</Col>
        <Col className="text-left">
          <Link href="/admin/categories/add-category">
            <a>
              <Button variant="success">افزودن دسته بندی</Button>
            </a>
          </Link>
        </Col>
      </Row>
      <Row>
        <Col>
          <Table responsive="xl">
            <thead>
              <tr>
                <th>#</th>
                <th>تصویر</th>
                <th>عنوان</th>
                <th>والد</th>
                <th>تعداد مطالب</th>
                <th></th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {list
                .filter((item) => item)
                .map((item, index) => {
                  return (
                    <tr key={item.id}>
                      <td>{index + 1}</td>
                      <td>{/* <img src={`/${item.image}`} alt=""/> */}</td>
                      <td>{item.title}</td>
                      <td> --- </td>
                      <td> --- </td>
                      <td>
                        <Button variant="link">ویرایش</Button>
                      </td>
                      <td>
                        <Button variant="link">حذف</Button>
                      </td>
                    </tr>
                  );
                })}
            </tbody>
          </Table>
        </Col>
      </Row>
    </Container>
  );
};
export default Category;

// created_at: "2020-12-03T11:01:24Z"
// desc: "توضیحات"
// id: 4
// image: "/media/cateforyimage/are1_EJ1XXOq.jpg"
// title: "دسته بندی 2"
// updated_at: "2021-01-06T16:24:02Z"
// wall: "sad"
