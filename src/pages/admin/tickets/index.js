import { useEffect, useState } from 'react';
import { tickets } from '../../../services';
import { Container, Row, Col, Table, Button } from 'react-bootstrap';

const Ticket = () => {
	const [list, setList] = useState([]);
	useEffect(() => {
		getList();
	}, []);
	const getList = async () => {
		try {
			const { data } = await tickets();
			setList(data.data);
		} catch (error) {
		} finally {
		}
	};
	return (
		<Container>
			<Row className="mb-6 align-items-center justify-content-between">
				<Col>search</Col>
				<Col className="text-left">
					<Button variant="success">افزودن</Button>
				</Col>
			</Row>
			<Row>
				<Col>
					<Table responsive="xl">
						<thead>
							<tr>
								<th>#</th>
								<th>نام</th>
								<th>ایمیل</th>
								<th>موبایل</th>
								<th>وضعیت</th>
								<th>پیام</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							{list
								.filter((item) => item)
								.map((item, index) => {
									return (
										<tr key={item.id}>
											<td>{index + 1}</td>
											<td> {item.user} </td>
											<td> --- </td>
											<td> --- </td>
											<td> {item.status} </td>
											<td>
												<Button variant="link">مشاهده</Button>
											</td>
											<td>
												<Button variant="link">حذف</Button>
											</td>
										</tr>
									);
								})}
						</tbody>
					</Table>
				</Col>
			</Row>
		</Container>
	);
};
export default Ticket;

// created_at: "2020-12-22T09:26:08Z"
// id: 11
// status: "پاسخ داده شده"
// title: "fhgf"
// user: "mehman22"
