import React, { useState } from "react";
import { Form, Row, Col, Alert } from "react-bootstrap";
import Link from "next/link";
import { useRouter } from "next/router";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { userForgotPassSchema } from "../../configs/formValidateSchema";

//components

import AuthTemplate from "../../components/templates/auth/AuthTemplate";
import CustomizeInput from "../../components/form/input/Input";
import CustomizeButton from "../../components/form/button/Button";
import {
  Person,
  Lock,
  Telephone,
  Envelope,
  PersonCheck,
} from "react-bootstrap-icons";

const ForgotPassword = () => {
  const router = useRouter();
  const [isLoading, setIsLoading] = useState(false);
  const [serverError, setServerError] = useState();
  const { handleSubmit, register, errors } = useForm({
    resolver: yupResolver(userForgotPassSchema),
  });

  const sendRequest = async (vals) => {

  };

  return (
    <>
      <AuthTemplate title="فراموشی رمز عبور" image="/images/Forgot-password-rafiki.png">
        <Col xs={12}>
          {serverError ? (
            <Alert
              variant="danger"
              onClose={() => setServerError("")}
              dismissible
            >
              {serverError}
            </Alert>
          ) : null}
          <Form className="d-flex flex-wrap row">
            <Col lg={12}>
              <CustomizeInput
                icon={() => <Person />}
                label="نام کاربری  "
                size="md"
                name="email"
                ref={register()}
                errorMessage={
                  (errors.email && errors.email.messages) ||
                  (serverError && serverError.email)
                }
              />
            </Col>
            <Col lg={12}>
              <CustomizeButton
                isFullwidth
                variant="success"
                size="md"
                className="my-3 submit logRegSubmit"
                onClick={handleSubmit(sendRequest)}
                isLoading={isLoading}
              >
                بازیابی رمز عبور
              </CustomizeButton>
            </Col>
          </Form>
          <div className="text-center text-darkBlue ">
             
            <span>
              <Link href="/user/login">
                <a className="text-dark">بازگشت</a>
              </Link>
            </span>
          </div>
        </Col>
      </AuthTemplate>
    </>
  );
};
export default ForgotPassword;
