import { useState, useEffect } from "react";
import Styles from "../../styles/profile.module.scss";
import { Container, Row, Col, Alert, Button } from "react-bootstrap";
import { Person, Lock, Envelope } from "react-bootstrap-icons";
import withPrivateRoute from "../../configs/withPrivateRoute";
import { useForm } from "react-hook-form";
//store action
import { useSelector } from "react-redux";

//components
import UpdateInfoForm from "../../components/user-panel/profile-forms/UpdateInfoForm";
import UpdatePasswordForm from "../../components/user-panel/profile-forms/UpdatePasswordForm";
import CustomizeInput from "../../components/form/input/Input";
import CustomizeButton from "../../components/form/button/Button";
import TitlePanel from "../../components/title-panel/TitlePanel";
import CustomizeAlert from "../../components/form/alert/Alert";
const Profile = () => {
  const state = useSelector((state) => state.user);
  const [serverError, setServerError] = useState("");
  const [serverMessage, setServerMessage] = useState("");
  return (
    <Container>
      {serverMessage ? (
        <CustomizeAlert variant="success" onClose={() => setServerMessage("")} dismissible>
          <p>{serverMessage}</p>
        </CustomizeAlert>
      ) : null}
      <Row className={`${Styles.profile} my-3 justify-content-center`}>
        <Col lg={6} md={6} sm={12}>
          <TitlePanel title="مشخصات کاربری" />
          <UpdateInfoForm successHandle={(msg) => setServerMessage(msg)} />
        </Col>
        <Col lg={6} md={6} sm={12}>
          <TitlePanel title="تغییر رمز عبور" />
          <UpdatePasswordForm successHandle={(msg) => setServerMessage(msg)}/>
        </Col>
      </Row>
    </Container>
  );
};

export default withPrivateRoute(Profile);
