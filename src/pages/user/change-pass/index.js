import React, { useState } from "react";
import { Form, Row, Col, Alert } from "react-bootstrap";
import { useRouter } from "next/router";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { userChangePassSchema } from "../../../configs/formValidateSchema";

//components
import AuthTemplate from "../../../components/templates/auth/AuthTemplate";
import CustomizeInput from "../../../components/form/input/Input";
import CustomizeButton from "../../../components/form/button/Button";
import { Lock } from "react-bootstrap-icons";

const ChangePassword = () => {
  const router = useRouter();
  const [isLoading, setIsLoading] = useState(false);
  const [serverError, setServerError] = useState();
  const { handleSubmit, register, errors } = useForm({
    resolver: yupResolver(userChangePassSchema),
  });

  const onChangePass = async (vals) => {};

  return (
    <>
      <AuthTemplate title="تغییر رمز عبور" image="/images/Forgot-password-rafiki.png">
        <Col xs={12}>
          {serverError ? (
            <Alert
              variant="danger"
              onClose={() => setServerError("")}
              dismissible
            >
              {serverError}
            </Alert>
          ) : null}
          <Form className="d-flex flex-wrap row">
            <Col lg={12}>
              <CustomizeInput
                icon={() => <Lock />}
                label="رمز عبور جدید "
                size="md"
                name="password"
                ref={register()}
                errorMessage={
                  (errors.password && errors.password.messages) ||
                  (serverError && serverError.password)
                }
              />
              <CustomizeInput
                icon={() => <Lock />}
                label="تکرار رمز عبور "
                size="md"
                name="password2"
                ref={register()}
                errorMessage={
                  (errors.password2 && errors.password2.messages) ||
                  (serverError && serverError.password2)
                }
              />
            </Col>
            <Col lg={12}>
              <CustomizeButton
                isFullwidth
                variant="success"
                size="md"
                className="my-3 submit logRegSubmit"
                onClick={handleSubmit(onChangePass)}
                isLoading={isLoading}
              >
                ذخیره
              </CustomizeButton>
            </Col>
          </Form>
        </Col>
      </AuthTemplate>
    </>
  );
};
export default ChangePassword;
