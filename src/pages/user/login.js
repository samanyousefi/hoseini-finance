import { useState ,useEffect} from "react";
import { Form, Row, Col, Alert } from "react-bootstrap";
import Link from "next/link";
import Head from "next/head";
import { useRouter } from "next/router";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { userLoginSchema } from "../../configs/formValidateSchema";
import { loginUser } from "../../services";
//store action
import { useDispatch,useSelector } from "react-redux";
import { setTokenAction } from "../../store/user/actions";
//components
import AuthTemplate from "../../components/templates/auth/AuthTemplate";
import CustomizeInput from "../../components/form/input/Input";
import CustomizeButton from "../../components/form/button/Button";
import CustomizeAlert from "../../components/form/alert/Alert";
import { Person, Lock } from "react-bootstrap-icons";

const Login = () => {
  const router = useRouter();
  const state = useSelector(state => state.user)
  const dispatch = useDispatch();
  const [isLoading, setIsLoading] = useState(false);
  const [rememberMe, setrememberMe] = useState(false);
  const [serverError, setServerError] = useState();
  const { handleSubmit, register, errors } = useForm({
    resolver: yupResolver(userLoginSchema),
  });
  useEffect(() => {
    if(state && state.access){
      router.push("/user/profile")
    }
  }, [])
  const onLogin = async (vals) => {
    vals = { ...vals, remember_me: rememberMe };
    setIsLoading(true);
    try {
      const { status, data } = await loginUser(vals);
      if (status === 200 && data.success) {
        localStorage.setItem("token", data.data.access);
        dispatch(setTokenAction(data.data));
        router.push("/user/profile");
      }
    } catch (error) {
      // console.log(error.response.data.message.non_field_errors[0])
      // console.log(error.request)
      // setErrorServer(error.response.data.message.non_field_errors[0]);
    } finally {
      setIsLoading(false);
    }
  };
  return (
    <>
      <Head>
        <title>Login | ورود</title>
      </Head>
      <AuthTemplate title="ورود به" image="/images/Forgot-password-rafiki.png" hiddenHs>
        <Col lg={11}>
          {serverError ? (
            <CustomizeAlert
              variant="danger"
              onClose={() => setServerError("")}
              dismissible
            >
              {serverError}
            </CustomizeAlert>
          ) : null}
          <Form className="d-flex flex-wrap row">
            <Col lg={12}>
              <CustomizeInput
                icon={() => <Person />}
                label="نام کاربری "
                name="username"
                ref={register()}
                size="md"
                errorMessage={
                  (errors.username && errors.username.message) ||
                  (serverError && serverError.username)
                }
              />
            </Col>
            <Col lg={12}>
              <CustomizeInput
                type="password"
                icon={() => <Lock />}
                label="رمز عبور"
                name="password"
                ref={register()}
                size="md"
                errorMessage={
                  (errors.password && errors.password.message) ||
                  (serverError && serverError.password)
                }
              />
            </Col>

            <Col
              lg={12}
              className="d-flex sm-text text-darkBlue align-items-center"
            >
              <Form.Check
                type="checkbox"
                id="remembre_me"
                onClick={() => setrememberMe((prev) => !prev)}
                checked={rememberMe}
              />
              من را به خاطر بسپار
            </Col>
            <Col lg={12} className="text-primary text-left sm-text">
              <Link href="/user/forgot-pass">
                <a>فراموشی رمز عبور</a>
              </Link>
            </Col>

            <Col lg={12}>
              <CustomizeButton
                isFullwidth
                size="md"
                variant="success"
                className="my-3 submit logRegSubmit"
                onClick={handleSubmit(onLogin)}
                isLoading={isLoading}
              >
                ورود
              </CustomizeButton>
            </Col>
          </Form>
          <div className="text-center text-darkBlue ">
            کاربر جدید هستید؟{`  `}
            <span>
              <Link href="/user/register">
                <a className=" text-green">ثبت نام</a>
              </Link>
            </span>
          </div>
        </Col>
      </AuthTemplate>
    </>
  );
};
export default Login;
