import { useEffect, useState, useRef, Fragment } from "react";
import { Container, Row, Col, Button } from "react-bootstrap";
import { Paperclip, Image, X } from "react-bootstrap-icons";
import { useRouter } from "next/router";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { ticketChatSchema } from "../../../../configs/formValidateSchema";
import { sendResponseTicket, userTicketList } from "../../../../services";
import { convertDate } from "../../../../configs/utils";
import withPrivateRoute from "../../../../configs/withPrivateRoute";

//components
import CustomizeInput from "../../../../components/form/input/Input";
import CustomizeButton from "../../../../components/form/button/Button";
import CustomizeFileButton from "../../../../components/form/file-Button/FileButton";
import TitlePanel from "../../../../components/title-panel/TitlePanel";
import Styles from "../../../../styles/TicketChat.module.scss";
import { Scrollbar } from "react-scrollbars-custom";

const TicketDetail = (props) => {
  const { query } = useRouter();
  const scrollRef = useRef();
  const { register, handleSubmit, errors, reset } = useForm({
    resolver: yupResolver(ticketChatSchema),
  });
  const [messages, setMesssage] = useState([]);
  const [images, setImages] = useState([]);
  let formData = new FormData();

  useEffect(() => {
    let chatBox = document.getElementsByClassName(
      "ScrollbarsCustom-Scroller"
    )[1];
    chatBox.scrollTop = chatBox.scrollHeight;
  }, [messages]);
  useEffect(() => {
    getMessages();
  }, [query]);

  const getMessages = async () => {
    try {
      const { status, data } = await userTicketList(query.id);
      if (status === 200 && data.success) {
        setMesssage(data.data);
      }
    } catch (error) {}
  };
  const sendResponse = async ({ message, images }) => {
    formData.append("message", message);
    if (images && images.length) formData.append("images[0]", images[0]);
    try {
      const { status } = await sendResponseTicket(query.id, formData);
      if (status === 201) {
        getMessages();
        reset({
          message: "",
        });
      }
    } catch (error) {
    } finally {
    }
  };
  const handleAddImage = (name) => {
    console.log(name);
    setImages((prev) => [...prev, name]);
  };
  return (
    <Container>
      <Row className="justify-content-between">
        <Col lg={12}>
          <TitlePanel title="جزئیات تیکت" />
        </Col>
      </Row>

      <div className={Styles.ticketChat}>
        <div
          className={`${Styles.ticketChatHader} d-flex flex-wrap justify-content-between `}
        >
          <Col lg={6} md={6} sm={12} className={Styles.title}>
            <span className="text-green">عنوان تیکت :</span>
            <span> {query.title} </span>
          </Col>
          <Col
            lg={6}
            md={6}
            sm={12}
            className={`${Styles.date} text-left FaNum`}
          >
            <span className="text-green">تاریخ ایجاد : </span>
            {messages && messages.length ? (
              <span className="FaNum">
                {convertDate(messages[0].created_at, "date")}
              </span>
            ) : null}
          </Col>
        </div>
        <div className={`${Styles.ticketChatBody} d-flex flex-wrap pl-2`}>
          <Scrollbar ref={scrollRef} style={{ height: 200 }}>
            <Col lg={12}>
              {messages && messages.length
                ? messages.map((msg, index) => {
                    return (
                      <Fragment key={index}>
                        <div
                          className={
                            msg.role === "admin"
                              ? Styles.answer
                              : Styles.question
                          }
                        >
                          <div className={Styles.desc}>
                            {msg.message}
                            <p className={`${Styles.date} mt-2 FaNum`}>
                              {convertDate(msg.created_at)}
                            </p>
                          </div>
                        </div>
                        {msg.images && msg.images.length
                          ? msg.images.map((img, index) => {
                              return (
                                <div
                                  key={index}
                                  className={
                                    msg.role === "admin"
                                      ? Styles.answer
                                      : Styles.question
                                  }
                                >
                                  <div className={Styles.desc}>
                                    <a
                                      target="_blank"
                                      href={`http://77.237.77.250:8080/${img}`}
                                    >
                                      <img
                                        src={`http://77.237.77.250:8080/${img}`}
                                      />
                                    </a>
                                    <p className={`${Styles.date} mt-2 FaNum`}>
                                      {convertDate(msg.created_at)}
                                    </p>
                                  </div>
                                </div>
                              );
                            })
                          : null}
                      </Fragment>
                    );
                  })
                : null}
            </Col>
          </Scrollbar>
        </div>
        <div
          className={`${Styles.ticketChatFooter} d-flex flex-wrap align-items-start`}
        >
          <Col className={Styles.submit}>
            <CustomizeButton
              size="sm"
              className={`${Styles.submitInp} `}
              isFullwidth
              onClick={handleSubmit(sendResponse)}
            >
              <img src="/icons/send.png" />
            </CustomizeButton>
          </Col>
          <Col className={Styles.inputs}>
            <CustomizeInput
              name="message"
              ref={register()}
              errorMessage={errors.message && errors.message.message}
            />
          </Col>
          <Col className={`${Styles.uploadFile} createTicketUpload`}>
            <CustomizeFileButton
              name="images"
              icon={() => <Paperclip />}
              className="custom-uploadInput"
              size="sm"
              ref={register()}
              handleSetImage={(name) => handleAddImage(name)}
            />
          </Col>
        </div>
        {/* {images && images.length ? (
          <div>
            <span onClick={() => setImages([])}>
              <X size={40} />
            </span>
            <span>
              <Image size={20} className="mx-5" />
            </span>
            <span>{images[0]}</span>
          </div>
        ) : null} */}
      </div>
    </Container>
  );
};
// TicketDetail.getInitialProps = (props) => {
//   console.log("----------------", query);
//   return {
//     query: props,
//     // id: query.id,
//     // title: query.title,
//   };
// };
export default withPrivateRoute(TicketDetail);
