import { useEffect, useState } from "react";
import Styles from "../../../styles/CreateTicket.module.scss";
import { Container, Row, Col, Button, Form } from "react-bootstrap";
import { ChatQuote, ChatText, Images } from "react-bootstrap-icons";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { createTicketSchema } from "../../../configs/formValidateSchema";
import { sendNewTicket } from "../../../services";
import withPrivateRoute from "../../../configs/withPrivateRoute";

//components
import CustomizeInput from "../../../components/form/input/Input";
import CustomizeButton from "../../../components/form/button/Button";
import CustomizeFileButton from "../../../components/form/file-Button/FileButton";
import TitlePanel from "../../../components/title-panel/TitlePanel";
import CustomizeAlert from "../../../components/form/alert/Alert";

const CreateTicket = () => {
  const [images, setImages] = useState([]);
  const [serverMessage, setServerMessage] = useState("");

  const { register, handleSubmit, errors } = useForm({
    resolver: yupResolver(createTicketSchema),
  });
  let formData = new FormData();

  const handleCreateTicket = async ({ title, message, images }) => {
    formData.append("title", title);
    formData.append("message", message);
    if (images && images.length) formData.append(`images[0]`, images[0]);
    try {
      const { status, data } = await sendNewTicket(formData);
      if (status === 201) {
        setServerMessage(data.message)
      }
    } catch (error) {
    } finally {
    }
  };

  const handleAddImage = (name) => {
    setImages((prev) => [...prev, name]);
  };
  return (
    <Container>
      <Row className="justify-content-between mb-3">
        <Col lg={12}>
          <TitlePanel title="تیکت جدید" />
        </Col>
      </Row>
      <Form>
      {serverMessage ? (
        <CustomizeAlert variant="success" onClose={() => setServerMessage("")} dismissible>
          <p>{serverMessage}</p>
        </CustomizeAlert>
      ) : null}
        <Row className={Styles.backgroundTicket}>
          <Col lg={12} className="mb-3">
            <span className="sm-text ">
              ما آنلاین پاسخگوی شما هستیم پیام خود را برای ما ارسال کنید
            </span>
          </Col>
          <Col lg={7}>
            <CustomizeInput
              icon={() => <ChatQuote />}
              label="عنوان پیام"
              name="title"
              ref={register()}
              errorMessage={errors.title && errors.title.message}
            />
          </Col>
          <Col lg={12}>
            <CustomizeInput
              icon={() => <ChatText />}
              label="متن پیام شما"
              rows={4}
              name="message"
              ref={register()}
              errorMessage={errors.message && errors.message.message}
            />
          </Col>
        </Row>
        <Row>
          <Col lg={3} md={3} xs={6}>
            <CustomizeButton
              isFullwidth
              size="sm"
              onClick={handleSubmit(handleCreateTicket)}
            >
              ارسال پیام
            </CustomizeButton>
          </Col>
          <Col lg={3} md={3} xs={6}>
            <CustomizeFileButton
              name="images"
              icon={() => <Images />}
              label="افزودن عکس"
              className="custom-uploadInput"
              size="sm"
              ref={register()}
            />
          </Col>
          <Col className="text-left px-4">
            {images.map((image, index) => {
              return <span key={index}>{image}</span>;
            })}
          </Col>
        </Row>
      </Form>
    </Container>
  );
};
export default withPrivateRoute(CreateTicket);
