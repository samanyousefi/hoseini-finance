//TO DO: redirect into login user
import { useRouter } from "next/router";
import { useEffect } from "react";
const UserPanel = () => {
  const router = useRouter();
  useEffect(() => {
    router.push("/user/login");
  }, []);
  return <></>; 
};
export default UserPanel;
