import { useState, useEffect } from "react";
import "../styles/global.scss";
import { useRouter } from "next/router";
import { createWrapper } from "next-redux-wrapper";
import { Provider } from "react-redux";
import { store, persistor } from "../store/store";
import { PersistGate } from "redux-persist/integration/react";
//components
import Layout from "../layout/Layout";
import PanelTemplate from "../components/templates/panel/PanelTemplate";
import AdminTemplate from "../components/templates/admin/AdminTemplate";
//package style
import "bootstrap/dist/css/bootstrap.min.css";
import "swiper/swiper.scss";
import "video.js/dist/video-js.css";


const userAuthForms = ["login", "register", "forgot-pass", "change-pass"];
function MyApp({ Component, pageProps }) {
  const [activeTheme, setActiveTheme] = useState("homeTheme");
  const { pathname } = useRouter();

  const handleChangeTheme = (currentPage) => {
    if (currentPage === "/") {
      return "homeTheme";
    } else if (
      ["user", "admin"].indexOf(currentPage.split("/").slice(1)[0]) > -1 &&
      currentPage.split("/").slice(1).length === 1
    ) {
      return "hidden";
    } else if (
      currentPage.split("/").slice(1)[0] === "user" &&
      userAuthForms.indexOf(currentPage.split("/").slice(1)[1]) < 0
    ) {
      return "panelTheme";
    } else if (
      currentPage.split("/").slice(1)[0] === "admin" &&
      userAuthForms.indexOf(currentPage.split("/").slice(1)[1]) < 0
    ) {
      return "adminTheme";
    } else if (currentPage.split("/").slice(1).length === 1) {
      return "staticTheme";
    } else if (
      currentPage.split("/").slice(1)[0] === "courses" &&
      currentPage.split("/").slice(1).length > 1 
    ) {
      return "staticTheme";
    } else {
      return "hidden";
    }
  };

  useEffect(() => {
    const currentTheme = handleChangeTheme(pathname);
    setActiveTheme(currentTheme);
  }, [pathname]);

  return (
    <>
      <Provider store={store}>
        <PersistGate persistor={persistor}>
          {activeTheme === "adminTheme" ? (
            <AdminTemplate>
              <Component {...pageProps} />
            </AdminTemplate>
          ) : (
            <Layout activeTheme={activeTheme}>
              {activeTheme === "panelTheme" ? (
                <PanelTemplate>
                  <Component {...pageProps} />
                </PanelTemplate>
              ) : (
                <Component {...pageProps} />
              )}
            </Layout>
          )}
        </PersistGate>
      </Provider>
    </>
  );
}

const makeStore = () => store;
export const wrapper = createWrapper(makeStore);

export default wrapper.withRedux(MyApp);

// useEffect(() => {
//     if (pathname === "/") {
//       //homeTheme.indexOf(pathname) > -1
//       setActiveTheme("homeTheme");
//     } else if (hiddenTheme.indexOf(pathname) > -1) {
//       setActiveTheme("hidden");
//       // } else if (panelTheme.indexOf(pathname) > -1) {
//     } else if (pathname.split("/").indexOf("user") > -1) {
//       //pathname.split("/").indexOf("user") > -1
//       setActiveTheme("panelTheme");
//     } else if (pathname.split("/")[1] === "admin") {
//       setActiveTheme("adminTheme");
//     } else {
//       setActiveTheme("staticTheme");
//     }
//   }, [pathname]);
